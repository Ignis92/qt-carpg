#include "CharacterRulesEditor.h"
#include "ui_CharacterRulesEditor.h"

#include <QFileDialog>
#include <QToolBar>

#include "CharacterRulesModel.h"

CharacterRulesEditor::CharacterRulesEditor(QWidget * parent, CharacterRulesModel * model)
	: QWidget(parent), ui(new Ui::CharacterRulesEditor), m_CharacterRulesModel(model)
{
	ui->setupUi(this);
	ui->tableView->setModel(m_CharacterRulesModel);

	// Toolbar setup
	auto toolBar = new QToolBar(this);
	toolBar->addAction(ui->actionLoad);
	toolBar->addAction(ui->actionSave);
	toolBar->addAction(ui->actionAdd);
	toolBar->addAction(ui->actionRemove);
	layout()->setMenuBar(toolBar);
}

CharacterRulesEditor::~CharacterRulesEditor()
{
	delete ui;
}

void CharacterRulesEditor::on_actionLoad_triggered()
{
	auto filename = QFileDialog::getOpenFileName(this,
												 tr("Open character rules"),
												 "",
												 tr("Plain text files: (*.txt)"));
	m_CharacterRulesModel->readFromFile(filename);
}

void CharacterRulesEditor::on_actionSave_triggered()
{
	auto filename = QFileDialog::getSaveFileName(this,
												 tr("Write a character rules file"),
												 "",
												 tr("Plain text files: (*.txt)"));
	m_CharacterRulesModel->writeToFile(filename);
}

void CharacterRulesEditor::on_actionAdd_triggered()
{
	int insertPosition = 0;

	if (!ui->tableView->selectionModel()->selectedIndexes().empty())
		insertPosition = ui->tableView->selectionModel()->selectedRows().last().row() + 1;

	m_CharacterRulesModel->insertRow(insertPosition);
}

void CharacterRulesEditor::on_actionRemove_triggered()
{
	// WARNING: duplicates -> possible bug?
	auto selectedList = ui->tableView->selectionModel()->selectedIndexes();
	std::sort(selectedList.begin(),
			  selectedList.end(),
			  [](const QModelIndex & a, const QModelIndex & b) { return a.row() > b.row(); });

	for (auto & elem : selectedList) {
		m_CharacterRulesModel->removeRow(elem.row());
	}
}

void CharacterRulesEditor::on_backButton_clicked()
{
	emit exit();
}
