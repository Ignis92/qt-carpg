#pragma once

#include <QWidget>

class QAbstractItemModel;
class QDataWidgetMapper;
class QLabel;
class QLineEdit;
// TODO: maybe add validator for "special" model
// class QValidator;

namespace Ui {
class LineEditWithDescription;
}

class LineEditWithDescription : public QWidget
{
	Q_OBJECT

public:
	explicit LineEditWithDescription(QWidget * parent = nullptr,
									 const QString & infoName = "",
									 QAbstractItemModel * model = nullptr /*,
									 const QValidator * validator = nullptr*/);
	~LineEditWithDescription();

	void setProperties(const QString & newInfoName,
					   QAbstractItemModel * newModel/*,
					   const QValidator * newValidator = nullptr*/);

private:
	Ui::LineEditWithDescription * ui;

	QDataWidgetMapper * mapper;
};

