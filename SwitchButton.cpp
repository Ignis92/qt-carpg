#include "SwitchButton.h"
#include "ui_SwitchButton.h"

SwitchButton::SwitchButton(QWidget * parent,
						   bool initialStatus,
						   const QString & trueText,
						   const QString & falseText)
	: QWidget(parent), ui(new Ui::SwitchButton)
{
	ui->setupUi(this);
	ui->trueButton->setText(trueText);
	ui->falseButton->setText(falseText);

	// NOTE: Redundant, emits the corresponding signal while constructed
	if (initialStatus)
		on_trueButton_clicked();
	else
		on_falseButton_clicked();
}

SwitchButton::~SwitchButton()
{
	delete ui;
}

void SwitchButton::on_trueButton_clicked()
{
	ui->trueButton->setEnabled(false);
	ui->falseButton->setEnabled(true);
	emit statusChanged(true);
}

void SwitchButton::on_falseButton_clicked()
{
	ui->falseButton->setEnabled(false);
	ui->trueButton->setEnabled(true);
	emit statusChanged(false);
}
