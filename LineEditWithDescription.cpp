#include "LineEditWithDescription.h"
#include "ui_LineEditWithDescription.h"

#include <QAbstractItemModel>
#include <QDataWidgetMapper>
// #include <QValidator>

LineEditWithDescription::LineEditWithDescription(QWidget * parent,
												 const QString & infoName,
												 QAbstractItemModel * model/*,
												 const QValidator * validator*/)
	: QWidget(parent), ui(new Ui::LineEditWithDescription), mapper(new QDataWidgetMapper(this))
{
	ui->setupUi(this);

	setProperties(infoName, model /*, validator*/);
}

LineEditWithDescription::~LineEditWithDescription()
{
	mapper->clearMapping();
	delete mapper;
	delete ui;
}

void LineEditWithDescription::setProperties(const QString & newInfoName,
											QAbstractItemModel * newModel/*,
											const QValidator * newValidator*/)
{
	ui->infoNameLabel->setText(newInfoName);

	if (newModel) {
		mapper->setModel(newModel);
		mapper->addMapping(ui->infoLineEdit, 0);
		mapper->setCurrentIndex(0);
	}

	// We change the validator only if explicitly supplied
	//if (newValidator)
	//	infoLineEdit->setValidator(newValidator);
}
