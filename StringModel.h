#pragma once

#include <QAbstractListModel>

class StringModel : public QAbstractListModel
{
	Q_OBJECT

public:
	explicit StringModel(QObject * parent = nullptr);

	// TODO: implement header for descriptive test?
	// Header:
	// QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;

	// bool setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role = Qt::EditRole) override;

	// Basic functionality:
	int rowCount(const QModelIndex &parent = QModelIndex()) const override;

	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

	// Editable:
	bool setData(const QModelIndex &index, const QVariant &value,
				 int role = Qt::EditRole) override;

	Qt::ItemFlags flags(const QModelIndex& index) const override;

private:
	QString m_data;
};

