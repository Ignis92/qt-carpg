#pragma once

#include <QWidget>

class CharacterRulesModel;

namespace Ui {
class CharacterRulesEditor;
}

class CharacterRulesEditor : public QWidget
{
	Q_OBJECT

public:
	explicit CharacterRulesEditor(QWidget * parent = nullptr, CharacterRulesModel * model = nullptr);
	~CharacterRulesEditor();

signals:
	void exit();

private slots:
	void on_actionLoad_triggered();
	void on_actionSave_triggered();
	void on_actionAdd_triggered();
	void on_actionRemove_triggered();
	void on_backButton_clicked();

private:
	Ui::CharacterRulesEditor * ui;

	CharacterRulesModel * const m_CharacterRulesModel;
};

