﻿#include "CharacterSheet.h"
#include "ui_CharacterSheet.h"

#include <QDebug>
#include <QGraphicsPixmapItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>

#include "CharacterSuperModel.h"
#include "LineEditWithDescription.h"

CharacterSheet::CharacterSheet(QWidget * parent, CharacterSuperModel * sheetData)
	: QWidget(parent), ui(new Ui::CharacterSheet), m_sheetData(sheetData)
{
	ui->setupUi(this);

	scene = new QGraphicsScene(this);
	ui->graphicsView->setScene(scene);

	connect(ui->switchButton, &SwitchButton::statusChanged, this, &CharacterSheet::changeMode);
}

CharacterSheet::~CharacterSheet()
{
	delete ui;
}

void CharacterSheet::on_backButton_clicked()
{
	emit exit();
}

void CharacterSheet::on_pushButton_clicked()
{
	auto newWidget = new LineEditWithDescription(nullptr,
												 "Strength",
												 m_sheetData->getModel("Stregth"));
	auto newProxy = new QGraphicsProxyWidget();
	newProxy->setWidget(newWidget);

	auto newPixmap = new QGraphicsPixmapItem(newWidget->grab());
	newPixmap->setFlag(QGraphicsItem::ItemIsMovable);
	newPixmap->setFlag(QGraphicsItem::ItemIsSelectable);

	ui->graphicsView->scene()->addItem(newPixmap);
	m_itemVector.append(QPair<QGraphicsProxyWidget *, QGraphicsPixmapItem *>(newProxy, newPixmap));
}

void CharacterSheet::changeMode(bool isEditableMode)
{
	if (isEditableMode) {
		qDebug() << "Request to change mode from normal to editable";
		for (auto const & elem : m_itemVector) {
			scene->removeItem(elem.first);
			scene->addItem(elem.second);
		}
	} else {
		qDebug() << "Request to change mode from editable to normal";
		for (auto const & elem : m_itemVector) {
			elem.first->setPos(elem.second->pos());
			scene->removeItem(elem.second);
			scene->addItem(elem.first);
		}
	}
	ui->pushButton->setEnabled(isEditableMode);
}
