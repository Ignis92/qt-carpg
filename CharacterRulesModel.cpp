#include "CharacterRulesModel.h"

#include <QDebug>
#include <QFile>

#include "CharacterSuperModel.h"
#include "StringModel.h"

CharacterRulesModel::CharacterRulesModel(QObject * parent) : QAbstractTableModel(parent) {}

QVariant CharacterRulesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if (role != Qt::DisplayRole)
		return QVariant();

	if (orientation == Qt::Horizontal) {
		switch (section) {
		case 0:
			return tr("Name");
		default:
			return QVariant();
		}
	}

	return QVariant();
}

int CharacterRulesModel::rowCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return 0;

	return m_components.size();
}

int CharacterRulesModel::columnCount(const QModelIndex &parent) const
{
	if (parent.isValid())
		return 0;

	return 1;
}

QVariant CharacterRulesModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole && role != Qt::EditRole)
		return QVariant();

	switch (index.column()) {
	case 0:
		return m_components.at(index.row());
	default:
		return QVariant();
	}
}

bool CharacterRulesModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (role != Qt::EditRole)
		return false;

	if (data(index, role) != value) {
		switch (index.column()) {
		case 0:
			m_components[index.row()] = value.toString();
			break;
		default:
			return false;
		}
		emit dataChanged(index, index, QVector<int>() << role);
		return true;
	}

	return false;
}

Qt::ItemFlags CharacterRulesModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return QAbstractTableModel::flags(index) | Qt::ItemIsEditable;
}

bool CharacterRulesModel::insertRows(int row, int count, const QModelIndex &parent)
{
	beginInsertRows(parent, row, row + count - 1);

	for (int i = 0; i < count; ++i)
		m_components.insert(row, "");

	endInsertRows();
	return true;
}

bool CharacterRulesModel::removeRows(int row, int count, const QModelIndex &parent)
{
	beginRemoveRows(parent, row, row + count - 1);

	for (int i = 0; i < count; ++i)
		m_components.removeAt(row);

	endRemoveRows();
	return true;
}

void CharacterRulesModel::readFromFile(QString filename)
{
	QFile inputFile(filename);

	if (!inputFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		qDebug() << "Something went wrong opening test file";
		return;
	}

	beginResetModel();

	m_components.clear();
	QTextStream inputStream(&inputFile);
	while (!inputStream.atEnd()) {
		QString line = inputStream.readLine();
		m_components.append(line);
	}

	endResetModel();
}

void CharacterRulesModel::writeToFile(QString filename)
{
	QFile outputFile(filename);

	if (!outputFile.open(QIODevice::WriteOnly | QIODevice::Text)) {
		qDebug() << "Something went wrong opening test file";
		return;
	}

	QTextStream outputStream(&outputFile);
	for (auto const & elem : m_components) {
		outputStream << elem << endl;
	}
}

CharacterSuperModel * CharacterRulesModel::buildStat() const
{
	auto result = new CharacterSuperModel();
	for (auto & component : m_components)
		result->modelDictionary.insert(component, new StringModel());

	return result;
}
