#include "StringModel.h"

StringModel::StringModel(QObject * parent) : QAbstractListModel(parent) {}

//QVariant StringModel::headerData(int section, Qt::Orientation orientation, int role) const
//{
//	if (role != Qt::DisplayRole) {
//		return QVariant();
//	}

//	if (orientation == Qt::Horizontal)
//		return "String"; // change this
//}

//bool StringModel::setHeaderData(int section, Qt::Orientation orientation, const QVariant &value, int role)
//{
//	if (value != headerData(section, orientation, role)) {
//		// Implement me!
//		emit headerDataChanged(orientation, section, section);
//		return true;
//	}
//	return false;
//}

int StringModel::rowCount(const QModelIndex &parent) const
{
	// For list models only the root node (an invalid parent) should return the list's size. For all
	// other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
	if (parent.isValid())
		return 0;

	return 1;
}

QVariant StringModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role != Qt::DisplayRole && role != Qt::EditRole)
		return QVariant();

	return m_data;
}

bool StringModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if (role != Qt::EditRole)
		return false;

	if (data(index, role) != value) {
		m_data = value.toString();
		emit dataChanged(index, index, QVector<int>() << role);
		return true;
	}

	return false;
}

Qt::ItemFlags StringModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return Qt::NoItemFlags;

	return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
}
