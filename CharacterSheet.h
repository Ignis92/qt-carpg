#pragma once

#include <QPair>
#include <QWidget>

class CharacterSuperModel;
class QGraphicsPixmapItem;
class QGraphicsProxyWidget;
class QGraphicsScene;
class QWidget;

namespace Ui {
class CharacterSheet;
}

class CharacterSheet : public QWidget
{
	Q_OBJECT

public:
	explicit CharacterSheet(QWidget * parent = nullptr, CharacterSuperModel * sheetData = nullptr);
	~CharacterSheet();

signals:
	void exit();

private slots:
	void on_backButton_clicked();
	void on_pushButton_clicked();

	void changeMode(bool isEditableMode);

private:
	Ui::CharacterSheet * ui;

	QSharedPointer<CharacterSuperModel> m_sheetData;
	QGraphicsScene * scene;

	QVector<QPair<QGraphicsProxyWidget *, QGraphicsPixmapItem *>> m_itemVector;
};

