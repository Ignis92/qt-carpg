#pragma once

#include <QHash>
#include <QString>

class QAbstractItemModel;

class CharacterRulesModel;

class CharacterSuperModel
{
public:
	CharacterSuperModel();

	QAbstractItemModel * getModel(const QString & dataName);

private:
	QHash<QString, QAbstractItemModel *> modelDictionary;

	friend CharacterRulesModel;
};

