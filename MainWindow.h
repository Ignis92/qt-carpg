#pragma once

#include <QMainWindow>

class CharacterRulesModel;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();

public slots:
	void backToMenu();

private slots:
	void on_editorButton_clicked();
	void on_sheetButton_clicked();

private:
	Ui::MainWindow * ui;

	CharacterRulesModel * const m_CharacterRulesModel;
};
