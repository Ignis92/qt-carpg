#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "CharacterRulesEditor.h"
#include "CharacterRulesModel.h"
#include "CharacterSheet.h"

MainWindow::MainWindow(QWidget * parent)
	: QMainWindow(parent), ui(new Ui::MainWindow),
	  m_CharacterRulesModel(new CharacterRulesModel(this))
{
	ui->setupUi(this);
}

MainWindow::~MainWindow()
{
	delete ui;
	delete m_CharacterRulesModel;
}

void MainWindow::backToMenu()
{
	ui->stackedWidget->setCurrentIndex(0);
	auto toDelete = ui->stackedWidget->widget(1);
	ui->stackedWidget->removeWidget(toDelete);
	delete toDelete;
}

void MainWindow::on_editorButton_clicked()
{
	auto * editor = new CharacterRulesEditor(this, m_CharacterRulesModel);
	ui->stackedWidget->insertWidget(1, editor);
	ui->stackedWidget->setCurrentIndex(1);

	connect(editor, &CharacterRulesEditor::exit, this, &MainWindow::backToMenu);
}

void MainWindow::on_sheetButton_clicked()
{
	auto * editor = new CharacterSheet(this, m_CharacterRulesModel->buildStat());
	ui->stackedWidget->insertWidget(1, editor);
	ui->stackedWidget->setCurrentIndex(1);

	connect(editor, &CharacterSheet::exit, this, &MainWindow::backToMenu);
}
