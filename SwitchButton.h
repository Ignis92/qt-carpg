#pragma once

#include <QString>
#include <QWidget>

namespace Ui {
class SwitchButton;
}

class SwitchButton : public QWidget
{
	Q_OBJECT

public:
	explicit SwitchButton(QWidget * parent = nullptr,
						  bool initialStatus = false,
						  const QString & trueText = "On",
						  const QString & falseText = "Off");
	~SwitchButton();

signals:
	void statusChanged(bool newStatus);

private slots:
	void on_trueButton_clicked();
	void on_falseButton_clicked();

private:
	Ui::SwitchButton * ui;
};

